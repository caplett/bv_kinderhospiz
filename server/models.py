""" Model definitions """
from django.db import models
from django.utils import timezone


class Person(models.Model):
    """
    Person Model
    Defines attributes of a Person
    """

    name = models.CharField(max_length=255)
    age = models.IntegerField()

    def get_name(self):
        """Get Name of Person"""
        return self.name

    def get_age(self):
        """Get Age of Person"""
        return self.age

    def __repr__(self):
        return self.name + " is added."


class SimpleImage(models.Model):
    """
    Dummy Modell to test simple api interactions with images.
    should be deprecated soon
    """

    image = models.ImageField(
        upload_to="images", default="server/assets/default_img.jpg"
    )
    idx = models.IntegerField()

    def get_image(self):
        """ Get Image of simple image object """
        return self.image

    def get_idx(self):
        """ Get idx of simple image object """
        return self.idx


class Category(models.Model):
    """
    Abstract class of Category of Article or Post
    """

    name = models.CharField(max_length=30)
    level = models.IntegerField()

    # def __str__(self):
        # if self.level == 1:
            # return "Level {}: {}".format(self.level, self.name)
        # return "Level {}: {} > {}".format(self.level, self.parent.name, self.name)

    class Meta:
        ordering = ["name"]


class PostCategory(Category):
    """
    Category of Post
    """

    parent = models.ForeignKey("self", blank=True, null=True, on_delete=models.CASCADE)

    def get_children(self):
        """ return children category that point to this post category object as their parent """
        children = self.postcategory_set.all()
        return children


class ArticleCategory(Category):
    """
    Category of Article
    """

    parent = models.ForeignKey("self", blank=True, null=True, on_delete=models.CASCADE)

    def get_children(self):
        """ return children category that point to this article category object as their parent """
        children = self.articlecategory_set.all()
        return children


class AbstractPost(models.Model):
    """
    Class of Category of Article
    """

    title = models.CharField(max_length=30, blank=True, null=True)
    date_posted = models.DateTimeField(default=timezone.now, blank=True, null=True)
    author = models.CharField(max_length=30)  # for now, in future:
    # author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    # image = models.ImageField(blank=True, null=True,
        # upload_to="images", default="server/assets/default_img.jpg"
    # )
    image = models.CharField(max_length=100, blank=True, null=True)
    subtitle = models.CharField(max_length=100, blank=True, null=True)
    text = models.CharField(max_length=1000,blank=True, null=True)

    def get_image(self):
        """ Get Image of abstract post """
        return self.image


    def __str__(self):
        return self.title

    class Meta:
        ordering = ["date_posted"]


class Article(AbstractPost):
    """
    Article by admin
    """

    category = models.ForeignKey(
        ArticleCategory, blank=True, null=True, on_delete=models.SET_NULL
    )

    def get_category(self):
        """ Get category of article post """
        return self.category


class Post(AbstractPost):
    """
    Post by user
    """

    category = models.ForeignKey(
        PostCategory, blank=True, null=True, on_delete=models.SET_NULL
    )

    def get_category(self):
        """ Get category of post """
        return self.category


# Create your models here.
