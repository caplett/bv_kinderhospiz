""" Tests for Person Model """
from django.test import TestCase
from django.core.files import File
from ..models import Person
from ..models import SimpleImage
from ..models import PostCategory
from ..models import ArticleCategory
from ..models import Article
from ..models import Post


class PersonTest(TestCase):
    """ Test module for Person model """

    def setUp(self):
        """ Setup a Testing Environment """
        Person.objects.create(name="Casper", age=3)
        Person.objects.create(name="Muffin", age=1)

    def test_person_age(self):
        """ Test if Person model works as expected """
        person_casper = Person.objects.get(name="Casper")
        person_muffin = Person.objects.get(name="Muffin")
        self.assertEqual(person_casper.get_age(), 3)
        self.assertEqual(person_muffin.get_age(), 1)


class SimpleImageTest(TestCase):
    """ Test module for Simple Image Model """

    def setUp(self):
        """ Create a SimpleImage object with default image from server/assets/default_img.jpg """
        SimpleImage.objects.create(idx=1)

    def test_image_is_same(self):
        """ Test if image is the same for simple image object and from folder"""
        image_1 = SimpleImage.objects.get(idx=1)

        default_img = open("server/assets/default_img.jpg")
        default_img_django = File(default_img)

        self.assertEqual(image_1.get_image(), default_img_django)


class PostCategoryTest(TestCase):
    """ Test module for PostCategory Model """

    def setUp(self):
        """ Setup testes for Post Category tests"""
        self.parent_category = PostCategory.objects.create(
            name="parent category", level=0
        )
        self.child_category = PostCategory.objects.create(
            name="child category", level=1, parent=self.parent_category
        )

    def test_get_children_function(self):
        """ This tests checks if getchildren returns all post categories below this one """

        children = self.parent_category.get_children()
        self.assertEqual(children.first(), self.child_category)


class ArticleCategoryTest(TestCase):
    """ Test module for ArticleCategory Model """

    def setUp(self):
        """ Setup testes for article category tests"""
        self.parent_category = ArticleCategory.objects.create(
            name="parent category", level=0
        )
        self.child_category = ArticleCategory.objects.create(
            name="child category", level=1, parent=self.parent_category
        )

    def test_get_children_function(self):
        """ This tests checks if getchildren returns all article categories below this one """

        children = self.parent_category.get_children()
        self.assertEqual(children.first(), self.child_category)


class ArticleTest(TestCase):
    """ Test module for Article Model """

    def setUp(self):
        """ setup testes for article tests"""
        self.category = ArticleCategory.objects.create(name="category", level=0)
        self.article = Article.objects.create(
            title="some article",
            author="a author",
            text="Lorem ipsum dolor sit amet",
            category=self.category,
        )

    def test_article(self):
        """ test if post get category and image works as expected """

        article = Article.objects.get(title="some article")
        self.assertEqual(self.category, article.get_category())

        # default_img = open("server/assets/default_img.jpg")
        # default_img_django = File(default_img)
        # self.assertEqual(article.get_image(), default_img_django)


class PostTest(TestCase):
    """ Test module for Post Model """

    def setUp(self):
        """ setup testes for post tests """
        self.category = PostCategory.objects.create(name="category", level=0)
        self.article = Post.objects.create(
            title="some post",
            author="a author",
            text="Lorem ipsum dolor sit amet",
            category=self.category,
        )

    def test_post(self):
        """ test if post works get category and image works as expected """

        post = Post.objects.get(title="some post")
        self.assertEqual(self.category, post.get_category())

        # default_img = open("server/assets/default_img.jpg")
        # default_img_django = File(default_img)
        # self.assertEqual(post.get_image(), default_img_django)
