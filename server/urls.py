""" Define Urls for Server app """
from django.conf.urls import url
from . import views

urlpatterns = [
        url(
            r'^api/v1/persons/(?P<pk>[0-9]+)$',
            views.get_delete_update_person,
            name='get_delete_update_person'
            ),
        url(
            r'^api/v1/persons/$',
            views.get_post_persons,
            name='get_post_persons'
            ),
        url(
            r'^api/v1/images/(?P<pk>[0-9]+)$',
            views.get_delete_put_simple_image,
            name='get_delete_put_simple_image'
            ),
        url(
            r'^api/v1/images/$',
            views.get_post_simple_images,
            name='get_post_simple_images'
            ),
        url(
            r'^api/v1/articles/$',
            views.get_post_articles,
            name='get_post_articles'
            ),
        url(
            r'^api/v1/posts/$',
            views.get_post_posts,
            name='get_post_posts'
            ),
        url(
            r'^api/v1/articles/(?P<pk>[0-9]+)$',
            views.put_delete_article,
            name='put_delete_article'
            ),
        # url(
            # r'^api/v1/posts/$',
            # views.get_post_posts,
            # name='get_post_posts'
            # ),
        # url(
            # r'^api/v1/posts/(?P<pk>[0-9]+)$',
            # views.put_delete_post,
            # name='put_delete_post'
            # ),

        url(
            r'^api/v1/articlecategories/$',
            views.get_post_article_categories,
            name='get_post_article_categories'
            ),
        url(
            r'^api/v1/postcategories/$',
            views.get_post_post_categories,
            name='get_post_post_categories'
            ),
        # url(
            # r'^api/v1/articles/(?P<pk>[0-9]+)$',
            # views.put_delete_article_category,
            # name='put_delete_article_category'
            # ),
        # url(
            # r'^api/v1/posts/$',
            # views.get_post_post_categories,
            # name='get_post_post_categories'
            # ),
        # url(
            # r'^api/v1/posts/(?P<pk>[0-9]+)$',
            # views.put_delete_post_category,
            # name='put_delete_post_category'
            # ),
        # url(
            # r'^api/v1/posts/(?P<pk>[0-9]+)$',
            # views.get_articles_from_category,
            # name='get_articles_from_category'
            # ),
        # url(
            # r'^api/v1/posts/(?P<pk>[0-9]+)$',
            # views.get_posts_from_category,
            # name='get_posts_from_category'
            # )

        ]
