FROM python:3.7

MAINTAINER caplett "bv_kinderhospiz@caplett.com"

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT ["./docker-entrypoint.sh"]
