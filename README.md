Coverage:
> https://codecov.io/gl/caplett/bv_kinderhospiz/branch/master

Documentation:
> https://caplett.gitlab.io/bv_kinderhospiz/master/

Deployment:
Deploy docker container with webhooks with: https://github.com/adnanh/webhook
CI lauchnes a webhook on the deployment server.

Build deployment image with:
> sudo docker build  bv_kinderhospiz_deploy deploy_webhook/.

Run deployment image with:
> docker run -v /var/run/docker.sock:/var/run/docker.sock bv_kinderhospiz_deploy
