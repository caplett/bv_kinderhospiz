#!/bin/sh
set -e

python manage.py makemigrations
python manage.py migrate

if [ -n  "$DJANGO_SUPERUSER_PASSWORD" ] && \
        [ -n  "$DJANGO_SUPERUSER_USERNAME" ] && \
        [ -n  "$DJANGO_SUPERUSER_EMAIL" ]
then
        python manage.py createsuperuser --noinput
fi

python manage.py runserver 0.0.0.0:8080

exec "$@"

