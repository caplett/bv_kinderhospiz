""" Configuration for django admin page """
from django.contrib import admin
from .models import Person, SimpleImage, Post, Article, PostCategory, ArticleCategory

admin.site.register((Person, SimpleImage, Post, Article, PostCategory, ArticleCategory))
