""" Configurations for Server app """
from django.apps import AppConfig

class ServerConfig(AppConfig):
    '''
    configuration for server.
    '''
    name = 'server'
