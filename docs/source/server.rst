server package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   server.tests

Submodules
----------

server.admin module
-------------------

.. automodule:: server.admin
   :members:
   :undoc-members:
   :show-inheritance:

server.apps module
------------------

.. automodule:: server.apps
   :members:
   :undoc-members:
   :show-inheritance:

server.models module
--------------------

.. automodule:: server.models
   :members:
   :undoc-members:
   :show-inheritance:

server.serializers module
-------------------------

.. automodule:: server.serializers
   :members:
   :undoc-members:
   :show-inheritance:

server.urls module
------------------

.. automodule:: server.urls
   :members:
   :undoc-members:
   :show-inheritance:

server.views module
-------------------

.. automodule:: server.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: server
   :members:
   :undoc-members:
   :show-inheritance:
