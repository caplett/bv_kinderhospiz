#!/bin/sh
set -e

export PATH="$PWD:${PATH}"

webhook --verbose

exec "$@"

