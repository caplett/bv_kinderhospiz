"""
Module defines all API Callbacks
"""

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import FileResponse
from .models import Person
from .models import SimpleImage
from .serializers import PersonSerializer
from .serializers import SimpleImageSerializer
from .serializers import ArticleSerializer
from .serializers import ArticleCategorySerializer
from .serializers import PostCategorySerializer
from .serializers import PostSerializer

from .models import ArticleCategory
from .models import Article
from .models import Post
from .models import PostCategory


@api_view(["GET", "DELETE", "PUT"])
def get_delete_update_person(request, pk):
    """ API callback functions for explicit Person objects """
    try:
        _ = Person.objects.get(pk=pk)
    except Person.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # get details of a single person
    if request.method == "GET":
        return Response({})

    # delte a single person
    if request.method == "DELETE":
        return Response({})

    # update details of a single person
    if request.method == "PUT":
        return Response({})

    # This case should not happen and should maybe be logged
    return Response(status=status.HTTP_501_NOT_IMPLEMENTED)


@api_view(["GET", "POST"])
def get_post_persons(request):
    """ API callback functions for all Person objects """
    # get all persons
    if request.method == "GET":
        persons = Person.objects.all()
        serializer = PersonSerializer(persons, many=True)
        return Response(serializer.data)

    # insert a new record for a person
    if request.method == "POST":
        return Response({})

    # This case should not happen and should maybe be logged
    return Response(status=status.HTTP_501_NOT_IMPLEMENTED)


@api_view(["GET", "DELETE", "PUT"])
def get_delete_put_simple_image(request, pk):
    """ simple API callback function to get, delete or update an image """

    idx = pk

    try:
        image_object = SimpleImage.objects.get(idx=idx)
    except SimpleImage.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        return FileResponse(image_object.image, content_type="application/image")

    if request.method == "PUT":
        data = {"image": request.data.get("image"), "idx": idx}

        serializer = SimpleImageSerializer(image_object, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == "DELETE":
        image_object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    return Response(status=status.HTTP_501_NOT_IMPLEMENTED)


@api_view(["GET", "POST"])
def get_post_simple_images(request):
    """ simple API callback function to get a list of all available images or add an new image """

    if request.method == "GET":
        images = SimpleImage.objects.all()
        serializer = SimpleImageSerializer(images, many=True)
        return Response(serializer.data)

    if request.method == "POST":
        data = {"image": request.data.get("image"), "idx": request.data.get("idx")}

        serializer = SimpleImageSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_501_NOT_IMPLEMENTED)


@api_view(["GET", "POST"])
def get_post_articles(request):
    """ Function to get all safed artifcles and add new article """

    if request.method == "GET":
        objects = Article.objects.all()
        serializer = ArticleSerializer(objects, many=True)
        return Response(serializer.data)

    if request.method == "POST":

        try:
            category = ArticleCategory.objects.get(
                pk=request.data.get("category", None)
            )
        except ArticleCategory.DoesNotExist:
            category = None

        Article.objects.create(
            title=request.data.get("title"),
            author=request.data.get("author"),
            image=request.data.get("image", None),
            subtitle=request.data.get("subtitle", None),
            text=request.data.get("text", None),
            category=category,
        )
        return Response(status=status.HTTP_201_CREATED)

    return Response(status=status.HTTP_501_NOT_IMPLEMENTED)


@api_view(["GET", "POST"])
def get_post_article_categories(request):
    """ Function to get all safed artifcles and add new article categorie"""

    if request.method == "GET":
        objects = ArticleCategory.objects.all()
        serializer = ArticleCategorySerializer(objects, many=True)
        return Response(serializer.data)

    if request.method == "POST":
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)

    return Response(status=status.HTTP_501_NOT_IMPLEMENTED)


@api_view(["GET", "POST"])
def get_post_posts(request):
    """ Function to get all safed artifcles and add new post """

    if request.method == "GET":
        objects = Post.objects.all()
        serializer = PostSerializer(objects, many=True)
        return Response(serializer.data)

    if request.method == "POST":
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)

    return Response(status=status.HTTP_501_NOT_IMPLEMENTED)


@api_view(["GET", "POST"])
def get_post_post_categories(request):
    """ Function to get all safed artifcles and add new post categorie"""

    if request.method == "GET":
        objects = PostCategory.objects.all()
        serializer = PostCategorySerializer(objects, many=True)
        return Response(serializer.data)

    if request.method == "POST":

        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)

    return Response(status=status.HTTP_501_NOT_IMPLEMENTED)




@api_view(["DELETE", "PUT"])
def put_delete_article(request, pk):
    """ Function to update existing articles and delete existing articles """
    try:
        article = Article.objects.get(pk=pk)
    except Article.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == "DELETE":
        article.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    if request.method == "PUT":
        data = request.data
        try:
            data["category"] = ArticleCategory.objects.get(
                pk=request.data.get("category", None)
            )
        except ArticleCategory.DoesNotExist:
            data["category"] = None

        serializer = ArticleSerializer(article, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response({})


# @api_view(["GET", "POST"])
# def get_post_article_categories(request):
# """ Function to get all article categories or add a new category """
# return Response({})


# @api_view(["DELETE", "PUT"])
# def put_delete_article_category(request, pk):
# """ Function to delete a article category or update a existing category """
# return Response({})


# @api_view(["GET"])
# def get_articles_from_category(request, pk):
# """ Function to get all articles from a article category existing category """
# return Response({})


# @api_view(["GET"])
# def get_posts_from_category(request, pk):
# """ Function to get all posts from a post category existing category """
# return Response({})


# @api_view(["GET", "POST"])
# def get_post_posts(request):
# """ Function to get all post categories or add a new post"""
# return Response({})


# @api_view(["DELETE", "PUT"])
# def put_delete_post(request, pk):
# """ Function to delete a post update a post """
# return Response({})


# @api_view(["GET", "POST"])
# def get_post_post_categories(request):
# """ Function to get all posts or add a new post"""
# return Response({})


# @api_view(["DELETE", "PUT"])
# def put_delete_post_category(request, pk):
# """ Function to delete a post category or update a post category """
# return Response({})
