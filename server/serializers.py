""" Define Serializers for Server app """
from rest_framework import serializers
from .models import Person
from .models import SimpleImage
from .models import ArticleCategory
from .models import PostCategory
from .models import Article
from .models import Post


class PersonSerializer(serializers.ModelSerializer):  # pylint: disable-msg=R0903
    """
    serializers for Person model
    """

    class Meta:  # pylint: disable-msg=R0903
        """
        metadata for Person model
        """

        model = Person
        fields = ("name", "age")


class SimpleImageSerializer(serializers.ModelSerializer):  # pylint: disable-msg=R0903
    """
    serializers for SimpleImage model
    """

    image = serializers.ImageField(use_url=False)

    class Meta:  # pylint: disable-msg=R0903
        """
        metadata for SimpleImage model
        """

        model = SimpleImage
        fields = ("image", "idx")


class ArticleSerializer(serializers.ModelSerializer):  # pylint: disable-msg=R0903
    """
    serializers for Article model
    """

    # image = serializers.ImageField(use_url=False, allow_null=True, required=False)

    class Meta:  # pylint: disable-msg=R0903
        """
        metadata for Article model
        """

        model = Article
        fields = (
            "pk",
            "title",
            "date_posted",
            "author",
            "image",
            "subtitle",
            "text",
            "category",
        )


class PostSerializer(serializers.ModelSerializer):  # pylint: disable-msg=R0903
    """
    serializers for Post model
    """

    # image = serializers.ImageField(use_url=False, allow_null=True, required=False)

    class Meta:  # pylint: disable-msg=R0903
        """
        metadata for Post model
        """

        model = Post
        fields = (
            "pk",
            "title",
            "date_posted",
            "author",
            "image",
            "subtitle",
            "text",
            "category",
        )


class ArticleCategorySerializer(
    serializers.ModelSerializer
):  # pylint: disable-msg=R0903
    """
    serializers for Article Category model
    """

    class Meta:  # pylint: disable-msg=R0903
        """
        metadata for Article Category model
        """

        model = ArticleCategory
        fields = ("name", "level", "parent", "pk")


class PostCategorySerializer(serializers.ModelSerializer):  # pylint: disable-msg=R0903
    """
    serializers for Post Category model
    """

    class Meta:  # pylint: disable-msg=R0903
        """
        metadata for Post Category model
        """

        model = PostCategory
        fields = ("name", "level", "parent", "pk")
