""" Tests for API callback functions """
import io
import json
from rest_framework import status
from rest_framework.test import APIClient
from django.test import TestCase, Client
from django.urls import reverse
from PIL import ImageChops, Image
from ..models import Person
from ..models import SimpleImage
from ..models import ArticleCategory
from ..models import Article
from ..serializers import PersonSerializer
from ..serializers import SimpleImageSerializer
from ..serializers import ArticleSerializer
from ..serializers import ArticleCategorySerializer
from ..serializers import PostSerializer
from ..models import PostCategory
from ..models import Post
from ..serializers import PostCategorySerializer

# initialize the API Client
client = Client()
api_client = APIClient()


class GetAllPersonsTest(TestCase):
    """ test module for GET all persons API """

    def setUp(self):
        """ Set up Testing Environment """
        Person.objects.create(name="Casper", age=3)
        Person.objects.create(name="Muffin", age=1)
        Person.objects.create(name="Rambo", age=2)
        Person.objects.create(name="Ricky", age=6)

    def test_get_all_persons(self):
        """ Run test to get all Perons in Database from API """
        # get API response
        response = client.get(reverse("get_post_persons"))
        # get data from db
        persons = Person.objects.all()
        serializer = PersonSerializer(persons, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetAllImagesTest(TestCase):
    """ test module for GET all images API """

    def setUp(self):
        """ Set up a Testing environment with some image objects """
        SimpleImage.objects.create(idx=1)
        SimpleImage.objects.create(idx=2)
        SimpleImage.objects.create(idx=3)

    def test_get_all_images(self):
        """ Run get all Images test """
        response = client.get(reverse("get_post_simple_images"))

        images = SimpleImage.objects.all()

        serializer = SimpleImageSerializer(images, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class PostNewImageTest(TestCase):
    """ test module for POST image API """

    def setUp(self):
        """ Set up new image json objects """

        self.valid_payload = {
            "image": (
                "name_img",
                open("server/assets/test_image.jpg", "rb"),
                "application/octet-stream",
            ),
            "idx": 5,
        }

        self.invalid_payload = {"idx": 6}

    def test_post_valid_image(self):
        """ Run post valid image test and check if image is the same """

        response = client.post(
            reverse("get_post_simple_images"), data=self.valid_payload
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Check it the two images are the same:
        # source:
        # https://stackoverflow.com/questions/1927660/compare-two-images-the-python-linux-way

        new_img_entry = SimpleImage.objects.get(idx=5)

        new_img = Image.open(new_img_entry.get_image())
        test_img = Image.open("server/assets/test_image.jpg")

        img_equal = ImageChops.difference(test_img, new_img).getbbox() is None

        self.assertTrue(img_equal)

    def test_post_invalid_image(self):
        """ Run post invalid image test """

        response = client.post(
            reverse("get_post_simple_images"), data=self.invalid_payload
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateImageTest(TestCase):
    """ test module for PUT image API """

    def setUp(self):
        """ Set up new image json objects """

        self.image_1 = SimpleImage.objects.create(idx=1)

        self.valid_payload = {
            "image": (
                "name_img",
                open("server/assets/test_image.jpg", "rb"),
                "application/octet-stream",
            ),
            "idx": 5,
        }
        self.invalid_payload_1 = {}
        self.invalid_payload_2 = {
            "image": (
                "name_img",
                open("server/assets/text.txt", "rb"),
                "application/octet-stream",
            )
        }

    def test_put_valid_payload(self):
        """ Run put valid image test and check if image got updated """

        response = api_client.put(
            reverse(
                "get_delete_put_simple_image", kwargs={"pk": self.image_1.get_idx()}
            ),
            format="multipart",
            data=self.valid_payload,
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Check it the two images are the same:
        # source:
        # https://stackoverflow.com/questions/1927660/compare-two-images-the-python-linux-way

        updated_img_entry = SimpleImage.objects.get(idx=self.image_1.get_idx())

        updated_img = Image.open(updated_img_entry.get_image())
        test_img = Image.open("server/assets/test_image.jpg")

        img_equal = ImageChops.difference(test_img, updated_img).getbbox() is None
        self.assertTrue(img_equal)

    def test_put_invalid_image_1(self):
        """ Run put invalid image test """

        response = api_client.put(
            reverse(
                "get_delete_put_simple_image", kwargs={"pk": self.image_1.get_idx()}
            ),
            data=self.invalid_payload_1,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_put_invalid_image_2(self):
        """ Run put invalid image test """

        response = api_client.put(
            reverse(
                "get_delete_put_simple_image", kwargs={"pk": self.image_1.get_idx()}
            ),
            data=self.invalid_payload_2,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_put_not_existing(self):
        """ Run put for not existing image test """

        response = client.put(
            reverse("get_delete_put_simple_image", kwargs={"pk": 10}),
            data=self.valid_payload,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class DeleteSimpleImageTest(TestCase):
    """ test module for deleting a simple image object """

    def setUp(self):
        """ create a SimpleImage entry to test delete api"""
        self.image_1 = SimpleImage.objects.create(idx=1)

    def test_valid_delete_image(self):
        """ test to delete a exisitng image entry"""
        response = api_client.delete(
            reverse(
                "get_delete_put_simple_image", kwargs={"pk": self.image_1.get_idx()}
            )
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        image_existst = True
        try:
            _ = SimpleImage.objects.get(idx=self.image_1.get_idx())
        except SimpleImage.DoesNotExist:
            image_existst = False

        self.assertFalse(image_existst)

    def test_invalid_delete_image(self):
        """ test to delete a not exisiting image entry"""
        response = api_client.delete(
            reverse("get_delete_put_simple_image", kwargs={"pk": 10})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetSimpleImageTest(TestCase):
    """ Test get api for simple image objects """

    def setUp(self):
        """ create some SimpleImage objects for testing"""

        self.image_1 = SimpleImage.objects.create(idx=1)
        self.image_2 = SimpleImage.objects.create(idx=2)
        self.image_3 = SimpleImage.objects.create(idx=3)

    def test_get_valid_single_image(self):
        """ test to get a exisiting SimpleImage object by idx"""
        response = client.get(
            reverse(
                "get_delete_put_simple_image", kwargs={"pk": self.image_1.get_idx()}
            )
        )

        # convert the stream into a image and compare them
        image_bytes = io.BytesIO(b"".join(response.streaming_content))
        image_received = Image.open(image_bytes)

        # Check it the two images are the same:
        # source:
        # https://stackoverflow.com/questions/1927660/compare-two-images-the-python-linux-way
        # TODO make this a function for less code duplication

        original_img_entry = self.image_1
        updated_img = Image.open(original_img_entry.get_image())

        img_equal = ImageChops.difference(image_received, updated_img).getbbox() is None

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(img_equal)

    def test_get_invalid_single_image(self):
        """ test to get an non existing SimpleImage object """
        response = api_client.get(
            reverse("get_delete_put_simple_image", kwargs={"pk": 10})
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class PostTests(TestCase):
    """ test module for all Post API """

    def setUp(self):
        """ Set up Testing Environment """
        self.category = PostCategory.objects.create(name="category", level=0)
        self.sub_category = PostCategory.objects.create(name="sub_category", level=1)

        self.post_1 = Post.objects.create(
            title="Post",
            author="a author",
            text="Lorem ipsum dolor sit amet",
            category=self.category,
        )
        self.post_2 = Post.objects.create(
            title="sub_Post",
            author=" author",
            text="Lorem ipsum dolor sit amet",
            category=self.category,
        )
        self.post_3 = Post.objects.create(
            title="sub_Post",
            author=" author",
            text="Lorem ipsum dolor sit amet",
            category=self.sub_category,
        )


    def test_get_all_post(self):
        """ Run test to get all Post in Database from API """
        response = client.get(reverse("get_post_posts"))

        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_post_categories(self):
        """ Run test to get all Post categories in Database from API """
        response = client.get(reverse("get_post_post_categories"))

        post_categories = PostCategory.objects.all()
        serializer = PostCategorySerializer(post_categories, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)



class ArticleTests(TestCase):
    """ test module for all article API """

    def setUp(self):
        """ Set up Testing Environment """
        self.category = ArticleCategory.objects.create(name="category", level=0)
        self.sub_category = ArticleCategory.objects.create(name="sub_category", level=1)

        self.article_1 = Article.objects.create(
            title="article",
            author="a author",
            text="Lorem ipsum dolor sit amet",
            category=self.category,
        )
        self.article_2 = Article.objects.create(
            title="sub_article",
            author=" author",
            text="Lorem ipsum dolor sit amet",
            category=self.category,
        )
        self.article_3 = Article.objects.create(
            title="sub_article",
            author=" author",
            text="Lorem ipsum dolor sit amet",
            category=self.sub_category,
        )

    def test_get_all_article(self):
        """ Run test to get all article in Database from API """
        response = client.get(reverse("get_post_articles"))

        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_article_categories(self):
        """ Run test to get all article categories in Database from API """
        response = client.get(reverse("get_post_article_categories"))

        article_categories = ArticleCategory.objects.all()
        serializer = ArticleCategorySerializer(article_categories, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # def test_get_articles_from_category(self):
        # """ Run test to get all article from a article category in Database from API """

        # articles = Article.objects.all(category=self.category)

        # response = client.get(
        # reverse("get_articles_from_category", kwargs={"pk": self.category.pk})
        # )
        # serializer = ArticleSerializer(articles, many=True)

        # self.assertEqual(response.data, serializer.data)
        # self.assertEqual(response.status_code, status.HTTP_200_OK)

    # def test_post_article_categories(self):
    # """ Run test to post a new article categoriy in Database from API """

    # payload = {"name": "sub_sub_category", "level": 2}

    # response = client.post(
    # reverse("get_post_article_categories"),
    # data=json.dumps(payload),
    # content_type="application/json",
    # )

    # sub_sub_category = ArticleCategory.objects.get(name=payload["name"])

    # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    # self.assertEqual(sub_sub_category.name, payload["name"])
    # self.assertEqual(sub_sub_category.level, payload["level"])

    # def test_update_article_categories(self):
    # """ Run test to update a article categoriy in Database from API """

    # payload = {"name": "other category"}

    # response = client.put(
    # reverse(
    # "put_delete_article_category", kwargs={"pk": self.category.pk}
    # ),
    # data=json.dumps(payload),
    # content_type="application/json",
    # )
    # self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
    # self.assertEqual(self.category.name, payload["name"])

    # def test_delete_existing_article_category(self):
    # """ Run test to delete a article categoriy in Database from API """
    # response = client.delete(
    # reverse(
    # "put_delete_article_category", kwargs={"pk": self.category.pk}
    # )
    # )
    # self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    # def test_delete_not_existing_article_category(self):
    # """ Run test to delete a not exisiting article categoriy in Database from API """
    # response = client.delete(
    # reverse("put_delete_article_category", kwargs={"pk": 20})
    # )
    # self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_article(self):
        """ Run test to put a new article in Database from API """

        payload = {
            "title": "new article",
            "author": "peter pan",
            "text": "labore ipsom",
            "category": self.category.pk,
        }

        response = client.post(
            reverse("get_post_articles"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        new_article = Article.objects.get(title="new article")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(new_article.title, payload["title"])
        self.assertEqual(new_article.author, payload["author"])
        self.assertEqual(new_article.text, payload["text"])
        self.assertEqual(new_article.category, self.category)

    def test_update_article(self):
        """ Run test to update a article in Database from API """

        payload = {
            "title": "new article",
            "author": "peter pan",
            "text": "labore ipsom",
            "category": self.category.pk,
        }

        response = client.put(
            reverse("put_delete_article", kwargs={"pk": self.article_1.pk}),
            data=json.dumps(payload),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            Article.objects.get(pk=self.article_1.pk).title, payload["title"]
        )

    def test_delete_existing_article(self):
        """ Run test to update a new article categories in Database from API """
        response = client.delete(
            reverse("put_delete_article", kwargs={"pk": self.article_1.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_not_existing_article(self):
        """ Run test to update a new article categories in Database from API """
        response = client.delete(reverse("put_delete_article", kwargs={"pk": 20}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
